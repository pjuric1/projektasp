# Credit Fraud Test

## Opis projekta

1. Projekt je napravljen u VS Code-u. Implementirana je klasa koja sadržava 
funkciju za učitavanje preko 500 000 zapisa iz .csv datoteke. Osim toga,
sadržava funkcije i za pretraživanje, dodavanje, brisanje i dohvaćanje 
jednog zapisa ili n zapisa (po ključu ili po vrijednosti). U main funkciji,
prikazani su primjeri za svaku navedenu funkcionalnost. 


2. Podatci su izvučeni iz 3 različita stupca: 
    - cc_num
    - unix_time
    - city_pop
    
3. U projektu je korištena mapa kao struktura podataka. Za svaki stupac 
je kreirana jedna mapa sa odgovarajućim ključem, a taj ključ
je broj retka iz .csv datoteke. 

4. Rezultati mjerenja:\
Za učitavanje podataka: 6740 ms\
Za ispis po ključu: 1 ms\
Za pretraživanje po ključu (2) : 9046 ms\
Za pretraživanje po vrijednosti : 7638 ms\
Za brisanje po ključu: 9129 ms\
Za brisanje po vrijednosti: 5615 ms\
Za dohvat najvećih: 368 ms\
Za dohvat najmanjih: 412 ms\
Za dodavanje: 5827 ms
5. Zaključak\
Mapa je dobra struktura podataka za brzo pretraživanje jer koristi pretraživanje
vrijednosti na temelju pripadnog ključa,
također, brisanje i dodavanje zapisa je isto jednostavno i brzo. U ovom projektu,
vrlo vjerojatno da bi i mjerenje bilo brže, da se ne čeka na input korisnika.



