#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

using namespace std;
using namespace chrono;

bool pretvorbaStringInt(string stringgg) {

  bool rezultat = true;
  try {

    int integer = stoi(stringgg); // mogucnost pretvorbe stringa u integer
  } catch (const invalid_argument &e) {
    rezultat = false;
  } catch (const out_of_range &e) {
    rezultat = false;
  }
  return rezultat;
}

class FraudTestPodatci {
public:
  // funkcija za ucitavanje podataka iz fraudTest.csv
  void ucitavanjePodatakaCSV() {
    fstream csvDat;
    csvDat.open("C:/Users/paola/Desktop/fraudTest.csv", ios::in);
    string red;
    int broj = 1;
    getline(csvDat, red);
    while (getline(csvDat, red)) {
      if (broj >= 1) {
        stringstream ss(red);
        string a;
        string cc_num;
        string unix_time;
        string city_pop;

        getline(ss, a, ',');
        getline(ss, a, ',');
        getline(ss, cc_num, ',');
        getline(ss, a, ',');
        getline(ss, a, ',');
        getline(ss, a, ',');
        getline(ss, a, ',');
        getline(ss, a, ',');
        getline(ss, a, ',');
        getline(ss, a, ',');
        getline(ss, a, ',');
        getline(ss, a, ',');
        getline(ss, a, ',');
        getline(ss, a, ',');
        getline(ss, a, ',');
        getline(ss, city_pop, ',');
        getline(ss, a, ',');
        getline(ss, a, ',');
        getline(ss, a, ',');
        getline(ss, unix_time, ',');

        // ako nije integer, ucitaj sljedeci stupac
        while (pretvorbaStringInt(unix_time) == false) {
          getline(ss, unix_time, ',');
        }

        // ako sadrzava slova, ucitaj sljedeci stupac
        if (unix_time.find('a') != string::npos || unix_time.find('e') != string::npos || unix_time.find('i') != string::npos || unix_time.find('o') != string::npos || unix_time.find('u') != string::npos || unix_time.find('b') != string::npos || unix_time.find('c') != string::npos || unix_time.find('d') != string::npos) {
          getline(ss, unix_time, ',');
        }

        mapa_ccNum[broj - 1] = stoll(cc_num);
        mapa_cityPop[broj - 1] = stoll(city_pop);
        mapa_unixTime[broj - 1] = stol(unix_time);
      }
      broj++;
    }
  }

  // funkcije za pretrazivanje po key-u
  void pretraziCc_numPoKljucu(int temp) {
    int key;
    for (int i = 0; i < temp; i++) {
      cout << "Unesite kljuc:" << endl;
      cin >> key;
      auto it = mapa_ccNum.find(key);
      if (it != mapa_ccNum.end()) {
        cout << "Element je pronaden." << endl;
      } else {
        cout << "Element ne postoji." << endl;
      }
    }
  }

  void pretraziCity_popPoKljucu(int temp) {
    int key;
    for (int i = 0; i < temp; i++) {
      cout << "Unesite kljuc:" << endl;
      cin >> key;
      auto it = mapa_cityPop.find(key);
      if (it != mapa_cityPop.end()) {
        cout << "Element je pronaden." << endl;
      } else {
        cout << "Element ne postoji." << endl;
      }
    }
  }

  void pronadiUnix_timePoKljucu(int temp) {
    int key;
    for (int i = 0; i < temp; i++) {
      cout << "Unesi kljuc: " << endl;
      cin >> key;
      auto it = mapa_unixTime.find(key);
      if (it != mapa_unixTime.end()) {
        cout << "Element s tim kljucem je pronaden." << endl;
      } else {
        cout << "Element ne postoji." << endl;
      }
    }
  }

  // funkcije za pretrazivanje po vrijednosti
  void pretraziCc_numPoVrijednosti(int temp) {
    unsigned long long int value;
    int broj;
    for (int i = 0; i < temp; i++) {
      broj = 0;
      cout << "Unesi vrijednost:" << endl;
      cin >> value;
      for (auto it = mapa_ccNum.begin(); it != mapa_ccNum.end(); it++) {
        if (value == it->second) {
          broj++;
        }
      }
      cout << "Postoji " << broj << " vrijednosti: " << value << endl;
    }
  }

  void pretraziCity_popPoVrijednosti(int temp) {
    unsigned long long int value;
    int broj;
    for (int i = 0; i < temp; i++) {
      broj = 0;
      cout << "Unesi vrijednost:" << endl;
      cin >> value;
      for (auto it = mapa_cityPop.begin(); it != mapa_cityPop.end(); it++) {
        if (value == it->second) {
          broj++;
        }
      }
      cout << "Postoji " << broj << " vrijednosti: " << value << endl;
    }
  }

  void pretraziUnix_timePoVrijednosti(int temp) {
    unsigned long long int value;
    int broj;
    for (int i = 0; i < temp; i++) {
      broj = 0;
      cout << "Unesi vrijednost:" << endl;
      cin >> value;
      for (auto it = mapa_unixTime.begin(); it != mapa_unixTime.end(); it++) {
        if (value == it->second) {
          broj++;
        }
      }
      cout << "Postoji " << broj << " vrijednosti: " << value << endl;
    }
  }

  // funkcije za dohvacanje najvecih
  void najveciCc_num() {
    unsigned long long int cc = 0;
    for (auto it = mapa_ccNum.begin(); it != mapa_ccNum.end(); it++) {
      if (it->second > cc) {
        cc = it->second;
      }
    }
    cout << "Najveci cc_num: " << cc << endl;
  }

  void najveciCity_pop() {
    unsigned long long int cityPop = 0;
    for (auto it = mapa_cityPop.begin(); it != mapa_cityPop.end(); it++) {
      if (it->second > cityPop) {
        cityPop = it->second;
      }
    }
    cout << "Najveci city_pop: " << cityPop << endl;
  }

  void najveciUnix_time() {
    unsigned long long int unixTime = 0;
    for (auto it = mapa_unixTime.begin(); it != mapa_unixTime.end(); it++) {
      if (it->second > unixTime) {
        unixTime = it->second;
      }
    }
    cout << "Najveci unix_time: " << unixTime << endl;
  }

  // funkcije za dohvacanje najmanjih
  void najmanjiCc_num() {
    unsigned long long int cc = mapa_ccNum.begin()->second;
    for (auto it = mapa_ccNum.begin(); it != mapa_ccNum.end(); it++) {
      if (it->second < cc) {
        cc = it->second;
      }
    }
    cout << "Najmanji cc_num: " << cc << endl;
  }

  void najmanjiCity_pop() {
    unsigned long long int cityPop = mapa_cityPop.begin()->second;
    for (auto it = mapa_cityPop.begin(); it != mapa_cityPop.end(); it++) {
      if (it->second < cityPop) {
        cityPop = it->second;
      }
    }
    cout << "Najmanji city_pop: " << cityPop << endl;
  }

  void najmanjiUnix_time() {
    unsigned long long int unixTime = mapa_unixTime.begin()->second;
    for (auto it = mapa_unixTime.begin(); it != mapa_unixTime.end(); it++) {
      if (it->second < unixTime) {
        unixTime = it->second;
      }
    }
    cout << "Najmanji unix_time: " << unixTime << endl;
  }

  // funkcije za brisanje
  void brisanjeKljuc(int temp) {
    int key;
    for (int i = 0; i < temp; i++) {
      cout << "Unesi kljuc za brisanje: " << endl;
      cin >> key;
      auto it = mapa_ccNum.find(key);
      mapa_ccNum.erase(it);
      auto it1 = mapa_cityPop.find(key);
      mapa_cityPop.erase(it1);
      auto it2 = mapa_unixTime.find(key);
      mapa_unixTime.erase(it2);
    }
  }

  void brisanjeCc_numVrijednost(int temp) {
    unsigned long long int vrijednost;
    int broj = 0;
    cout << "Unesite vrijednost za brisanje: ";
    cin >> vrijednost;
    for (auto it = mapa_ccNum.begin(); it != mapa_ccNum.end(); it++) {
      if (it->second == vrijednost) {
        it = mapa_ccNum.erase(it);
        broj++;
      }
    }
    cout << "Cc_num ukupno obrisano " << broj << " s vrijednoscu:  " << vrijednost << "." << endl;
  }

  void brisanjeCity_popVrijednost(int temp) {
    unsigned long long int vrijednost;
    int broj = 0;
    cout << "Unesite vrijednost za brisanje: ";
    cin >> vrijednost;
    for (auto it = mapa_cityPop.begin(); it != mapa_cityPop.end(); it++) {
      if (it->second == vrijednost) {
        it = mapa_cityPop.erase(it);
        broj++;
      }
    }
    cout << "City_pop ukupno obrisano " << broj << " s vrijednoscu:  " << vrijednost << "." << endl;
  }

  void brisanjeUnix_timeVrijednost(int temp) {
    unsigned long long int vrijednost;
    int broj = 0;
    cout << "Unesite vrijednost za brisanje: ";
    cin >> vrijednost;
    for (auto it = mapa_unixTime.begin(); it != mapa_unixTime.end(); it++) {
      if (it->second == vrijednost) {
        it = mapa_unixTime.erase(it);
        broj++;
      }
    }
    cout << "Unix_time ukupno obrisano " << broj << " s vrijednoscu:  " << vrijednost << "." << endl;
  }

  bool postojiLiKljuc(int key) {
    auto it = mapa_ccNum.find(key);
    auto it2 = mapa_cityPop.find(key);
    auto it3 = mapa_unixTime.find(key);
    if (it != mapa_ccNum.end() && it2 != mapa_cityPop.end() && it3 != mapa_unixTime.end()) {
      return true;
    }
    return false;
  }

  void postojiLiKljucIspis(int key) {
    if (postojiLiKljuc(key) == true) {
      cout << "Kljuc je : " << key << endl;
      cout << "Cc_num podatak za taj kljuc: " << mapa_ccNum[key] << endl;
      cout << "City_pop podatak za taj kljuc: " << mapa_cityPop[key] << endl;
      cout << "Unix_time podatak za taj kljuc: " << mapa_unixTime[key] << endl;
    }
  }

  // funkcije za dodavanje novih zapisa

  void dodajCc_numPoVrijednosti(int temp) {
    int trenutno = mapa_ccNum.size();
    for (int i = 1; i <= temp; i++) {
      unsigned long long int vrijednost;
      cout << "Unesi vrijednost: " << endl;
      cin >> vrijednost;
      mapa_ccNum[trenutno + temp] = vrijednost;
      cout << "Dodan novi zapis u cc_num." << endl;
    }
  }

  void dodajCity_popPoVrijednosti(int temp) {
    int trenutno = mapa_cityPop.size();
    for (int i = 1; i <= temp; i++) {
      unsigned long long int vrijednost;
      cout << "Unesi vrijednost: " << endl;
      cin >> vrijednost;
      mapa_cityPop[trenutno + temp] = vrijednost;
      cout << "Dodan novi zapis u city_pop." << endl;
    }
  }

  void dodajUnix_timePoVrijednosti(int temp) {
    int trenutno = mapa_unixTime.size();
    for (int i = 1; i <= temp; i++) {
      unsigned long long int vrijednost;
      cout << "Unesi vrijednost: " << endl;
      cin >> vrijednost;
      mapa_unixTime[trenutno + temp] = vrijednost;
      cout << "Dodan novi zapis unix_time." << endl;
    }
  }

private:
  map<int, unsigned long long int> mapa_ccNum;
  map<int, unsigned long long int> mapa_cityPop;
  map<int, unsigned long long int> mapa_unixTime;
};

int main() {
  FraudTestPodatci podatci;
  cout << "Citanje podataka iz csv-a: \n"
       << endl;

  // mjerenje vremena u milisekundama

  auto start = high_resolution_clock::now();
  podatci.ucitavanjePodatakaCSV(); // ucitavanje podataka
  auto stop = high_resolution_clock::now();
  auto duration = duration_cast<milliseconds>(stop - start);
  cout << "Vrijeme potrebno da se procitaju i unesi podaci iz csv-a : " << duration.count() << " ms " << endl;

  cout << "\nIspis podataka: " << "\n"
       << endl;
  start = high_resolution_clock::now();
  podatci.postojiLiKljucIspis(502101); // mjerenje postoji li kljuc i vrijeme za ispis
  stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  cout << "Vrijeme potrebno za ispis tih podataka: " << duration.count() << " ms " << endl;

  // primjer mjerenja pretrazivanja po kljucu
  cout << "\nPronadi cc_num" << endl;
  start = high_resolution_clock::now();
  podatci.pretraziCc_numPoKljucu(2); // hard kodirano da pretrazi dvaput
  stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  cout << "Vrijeme potrebno za cc_num podatke na odredenom kljucu: " << duration.count() << " ms " << endl;

  // primjer mjerenja pretrazivanja po vrijednosti
  cout << "\nPronadi city_pop" << endl;
  start = high_resolution_clock::now();
  podatci.pretraziCity_popPoVrijednosti(1);
  stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  cout << "Vrijeme potrebno za city pop koji imaju odredenu vrijednost: " << duration.count() << " ms " << endl;

  // primjer mjerenja brisanja po kljucu
  cout << "\nBrisanje " << endl;
  start = high_resolution_clock::now();
  podatci.brisanjeKljuc(2);
  stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  cout << "Vrijeme potrebno za brisanje po kljucu: " << duration.count() << " ms " << endl;

  // primjer mjerenja brisanja po vrijednosti
  cout << "\nBrisanje" << endl;
  start = high_resolution_clock::now();
  podatci.brisanjeCc_numVrijednost(1);
  stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  cout << "Vrijeme potrebno za brisanje cc_num po vrijednosti: " << duration.count() << " ms " << endl;

  // primjer mjerenja pretrazivanja najveceg
  cout << "\nDohvati najveceg" << endl;
  start = high_resolution_clock::now();
  podatci.najveciCity_pop();
  stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  cout << "Vrijeme potrebno da se pronadu city_pop podaci s najvecom vrijednosti: " << duration.count() << " ms " << endl;

  // primjer mjerenja pretrazivanja najmanjeg
  cout << "\nDohvati najmanjeg" << endl;
  start = high_resolution_clock::now();
  podatci.najmanjiCc_num();
  stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  cout << "Vrijeme potrebno da se pronadu cc_num podaci s najmanjom vrijednost: " << duration.count() << " ms " << endl;

  // primjer mjerenja dodavanja novog
  cout << "\nDodaj" << endl;
  start = high_resolution_clock::now();
  podatci.dodajCity_popPoVrijednosti(1);
  stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  cout << "Vrijeme potrebno da se dodaju city_pop podatci s odredenom vrijednoscu: " << duration.count() << " ms" << endl;
}
